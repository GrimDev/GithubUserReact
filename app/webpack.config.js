'use strict';

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const SRC = path.join(__dirname, '/src');
const DIST = path.join(SRC, '/dist');

module.exports = {
  entry: path.join(SRC, '/main.js'),
  output: {
    path: DIST,
    filename: 'bundle.js'
  },
  module: {
   loaders: [
      { test: /\.js?$/, exclude: /node_modules/, loader: 'babel?cacheDirectory' },
      { test: /\.less$/, loader: 'style!css!less' },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Github user React',
      template: path.join(SRC, 'index.ejs'),
      inject: 'body'
    }),
    new webpack.NoErrorsPlugin()
  ],
  devServer: {
    hot: true,
    inline: true,
    contentBase: SRC
  },
  devtool: 'source-map'
};
