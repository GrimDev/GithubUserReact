'use strict';

const githubUserApi = 'https://api.github.com/users';

export const fetchUser = (username) => {
  return new Promise((resolve, reject) => {
    fetch(`${githubUserApi}/${username}`)
    .then((response) => {
      response.json().then(user => resolve(user));
    })
    .catch(err => reject(err));
  });
};
