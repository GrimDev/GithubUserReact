'use strict';

export const FETCHING = 'FETCHING';
export const NOT_FOUND = 'NOT_FOUND';
export const USER_FOUND = 'USER_FOUND';
export const FETCH_USER = 'FETCH_USER';
