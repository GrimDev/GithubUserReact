'use strict';

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import { fetchUser } from '../../actions/actionCreators';
import DevTools from '../DevTools';

const App = React.createClass({
  propTypes: {
    dispatch: PropTypes.func.isRequired,
    fetching: PropTypes.bool.isRequired,
    notFound: PropTypes.bool.isRequired,
    user: PropTypes.object.isRequired
  },
  componentDidMount() {
    this.props.dispatch(fetchUser('grimdev'));
  },
  render() {
    const { fetching, notFound, user } = this.props;

    let renderElement;

    if(fetching) {
      renderElement = 'Fetching ...';
    } else if(notFound) {
      renderElement = 'User not found';
    } else {
      renderElement = (
        <div>
          <img src={user.avatar_url} height="230" width="230" />
          <h1>{user.login}</h1>
          <span className="userInfos">{user.name}@{user.company}</span>
        </div>
      );
    }

    return (
      <div>
        <div className="githubUser">{renderElement}</div>
        <DevTools />
      </div>
    );
  }
});

const mapStateToProps = (state) => {
  return {
    ...state.user
  };
};

export default connect(mapStateToProps)(App);
