'use strict';

import * as actionTypes from './actionTypes';

import * as githubUserService from '../services/githubUserService';

export const fetching = () => {
  return {
    type: actionTypes.FETCHING
  };
};

export const notFound = () => {
  return {
    type: actionTypes.NOT_FOUND
  };
};

export const userFound = (user) => {
  return {
    type: actionTypes.USER_FOUND,
    user
  };
};

export const fetchUser = (username) => {
  return (dispatch) => {
    dispatch(fetching());
    return githubUserService.fetchUser(username).then((user) => {
      dispatch(userFound(user));
    });
  };
};
