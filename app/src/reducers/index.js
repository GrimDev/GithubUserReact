'use strict';

import * as actionsTypes from '../actions/actionTypes';

const initalState = {
  fetching: false,
  notFound: false,
  user: {
    name: 'test',
    company: 'test',
    login: 'test'
  },
  username: 'grimdev'
};

export const user = (state = initalState, action) => {
  switch(action.type) {
    case actionsTypes.FETCHING:
      return {
        ...state,
        fetching: true
      };
      break;
    case actionsTypes.USER_FOUND:
      return {
        ...state,
        fetching: false,
        user: action.user
      };
      break;
    default:
      return state;
      break;
  }
};
